# 1. INITIATION GITLAB

Je n’ai pas pu suivre la séance du jeudi 30 septembre alors j’ai rattrapé le cours avec **Amandine**, merci à elle !
Nous avons procédé à la *création de la clé SSH* ainsi qu’au **clonage** de notre page web sur notre poste de travail.

Même si j’ai rencontré peu de difficultés au sein de cet exercice, je vais **documenter** les étapes d’installation de la clé ainsi que le clonage sur mon espace de travail personnel.

Mon ordinateur étant un **MacBook**, j’ai commencé par me rendre sur le *terminal* de mon ordinateur pour pouvoir entrer les lignes de **code**, seul moyen de **communiquer avec la machine**.

Avant de commencer l’installation de la clé, je vérifie si mon ordinateur possède déjà Git. Mon  ordinateur me répond que oui en me donnant la version que je possède.

![](../images/module 1/1.png)

Ensuite, il m’a suffit de suivre le tutoriel disponible sur GitLab.

## *Configurer Git*
Je commence par configurer Git, c’est comme « m’identifier » :
L’ordinateur confirme en redonnant le nom d’utilisateur et l’e-mail que j’ai associée à mon compte.  Je peux continuer.

![](../images/module 1/2.png)

## *Générer une clé SSH*
Il suffit de recopier la première ligne de code en entrant l’adresse mail (avec laquelle on s’est enregistré) entre guillemets  pour commencer à générer la clé ed25519 :
Après cela l’ordinateur propose d’entrer un mot de passe, on peut faire **ENTRÉE** pour faciliter.
On presse le touche **ENTRÉE** jusqu’à la finalisation.

 ![](../images/module 1/3.png)

L’ordinateur nous donne une sorte de tableau :

![](../images/module 1/4.png)

Ensuite on entre la commande :

![](../images/module 1/5.png)

Et on obtient notre clé :

![](../images/module 1/6.png)

On copie la clé que l’on vient de recevoir et on retourne sur le site Git pour l’y rentrer. Il faut suivre le chemin :
Icône de profile  **>** préférences **>** **SSH keys** **>** coller la clé dans l’encadré

## *Vérifier que l’on peut se connecter*
On va vérifier que notre clé SSH s’est enregistrée correctement (on laisse le temps à la machine le temps de se connecter ☝️) :

![](../images/module 1/7.png)

## *Clonage du Git sur son poste de travail*
On commence par choisir le lieu sur notre ordinateur où l’on va enregistrer notre travail.
On regarde comment sont appelés les dossiers de stockage de notre ordinateurs en rentrant la commande *ls*. L’ordinateur nous donne alors les noms des dossiers où l’ont va pouvoir stocker.

![](../images/module 1/8.png)

Pour cela, j’ai choisi de déposer sur le *Bureau*, je dis donc à ma machine que c’est dans *Desktop* que je veux aller :  

![](../images/module 1/9.png)

Ensuite je commence le processus de clonage :

Enseignements **>** 2021 - 2022 **>** FabZero - Design **>** **Clone** (bouton bleu) **>** clone with SSH (à copier)

Ensuite j’entre la commande git clone et je colle ce que je viens de copier :

![](../images/module 1/10.png)

La machine valide le processus de clonage, et c’est fini !

![](../images/module 1/11.png)

Vous pouvez voir apparaitre un dossier à votre **prénom.nom** apparaitre à l’endroit que vous aviez choisi :

## *Editer sa page web*

Pour éditer une page web il faut utiliser le langage Markdown.
Il y a certains signes qui permettent de coder, par exemple :
* Italic	_ x _
* Gras	** x **
* Entête	# x
* Insérer une image avec lien	! [x] (lien de l'image)
* Citation	> x
* Liste avec point	* + espace
* Liste avec chiffre	1. 2. 3.
* Paragraphe	double espace en fin de phrase

Pour modifier sa page web :
* sur gitlab cliquer sur son projet -> repository -> files -> docs -> index.md -> edit
* ensuite on peut supprimer les texte déjà écrits et commencer sa présentation en adaptant le texte et en ajoutant des images.


Aperçu & publication :
* Si on veut un aperçu de sa page il faut cliquer sur preview et pour revenir en mode édition en cliquant sur write. Lorsqu'on est prêt à publier sa page, on peut soumettre en appuyant sur commit changes.
* Pour visualiser sa page sur le site,  suivre le chemin : settings -> pages -> cliquer sur son prénom

Pour ajouter et compresser une image

* Suivre le chemin files -> docs -> images -> cliquer sur le + -> upload file
* dans le mode édition écrire : "![](images/nomdufichier.jpeg)"

Avec un mac on peut compresser une image depuis le mode *aperçu* en diminuant la taille, suivre le chemin : outils -> ajuster la taille, et ensuite choisir la taille souhaitée.
