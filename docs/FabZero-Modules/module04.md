# 4. DÉCOUPE ASSISTÉE PAR ORDINATEUR

Cette semaine, Alex nous a fait les formations des découpeuses laser et vinyle.

**(correction Léo : ajouter description des machines et leur champ d'utilisation)**

Les deux machines fonctionnent de la même manière elles lisent le dessin vectoriel. Alors que la découpeuse vinyle est faite pour découper du papier. La découpeuse laser peut découper plusieurs matériaux comme : **le bois, le contreplaqué, le PMMA, le plexiglas, le papier, le carton, le textile**. Mais chaque matériau aura des **propriétés de découpe** qui lui seront **propres**. Certains matériaux sont totalement **interdits** à la découpe car dangereux pour la santé ou car ils risquent de **provoquer un feu**. Il s'agit par exemple de : **ABS, PS, PE, PET, PP, les composants à base de fibres et les métaux**.
Toutes les consignes et le protocole de sécurité se trouvent **[ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines)** .

## LASERSAUR

## *Comment s'identifier sur la LASERSAUR ?*
Tout d'abord il faut savoir que cette machine fonctionne par **réservation**. Pour cela, il faut se rendre sur le site **[Fabman](https://fabman.io/members/1405/equipment)** et prendre rendez vous. Vous vous présentez à l'heure et vous scannez le QR code depuis votre espace personnel. Ensuite la machine s'active et vous pouvez commencer les configurations.

**ATTENTION :**

* 1/ la machine fonctionne uniquement pendant un laps de temps déterminé par le temps de découpe que vous avez réservé (2h max) si le fichiers n'est pas terminé à l'issu à la fin du temps réservé, la machine s'arrête et votre fichier ne pourra pas être relancé là où il s'est arrêté.

* 2/ Si vous ne commencez pas les découpes dans les 30 min après le début de votre horaire la réservation est annulée et quelqu'un pourra prendre votre place.

## *Comment lancer sa découpe ?*

Quand vous avez scanné, tournez le bouton rouge de la machine dans le sens des aiguilles d'une montre. Vous allez commencer par paramétrer votre dessin.  

* cliquez sur ouvrir et choisissez votre document.

  *ouverture de parenthèse*

**ASTUCE n°1 :**

Lorsque que l'on dessine depuis un logiciel comme **Archicad, Autocad, Affinity Designer ou Illustrator**, il faut exporter le fichier sous un format lisible par la machine, soit : **SVG, DFX ou DBA**. Au Fablab, pour retoucher son fichier si le logiciel Driveboard App de la découpeuse LASERSAUR ne le lit pas du premier coup, il faudra l'ouvrir à l'aide d'**Inkscape**. le logiciel n'est pas très difficile à prendre en main si l'on sait où chercher les outils nécessaires aux modifications de premiers recours. **Voici quelques problèmes que j'ai pu rencontrer et les commandes utiles pour les solutionner :**

Lorsque vous ouvrez votre fichier depuis Driveboard App, l'application peut faire apparaitre un message d'erreur ou alors votre fichier n'apparait pas. Vous allez être tenté de zoomer etc mais rien n'y fait. Alors lâchez votre CTRL + molette et suivez ce tuto :

* ouvrez votre fichier avec Inkscape.

* dans la barre d'outils en haut de la page, choisissez **mm** comme unité de mesure.

* Ensuite, dans la barre d'outil à droite, cliquez sur une petite flèche qui pointe vers la droite. un menu apparait choisissez paramètres du document. ici faites de meme et choisissez le **mm** comme unité de mesure et le **format** de votre feuille de travail.

* votre feuille re dimensionnée apparait sous votre projet et plus souvent il est plus grand ou plus petit que la feuille.

* sélectionnez votre projet entièrement. des flèches apparaissent à chaque coin, déplacez votre fichier dans un coin de la feuille de travail et ensuite enfoncez la touche ctrl en sélectionnant une des flèches sur le coin de votre projet sélectionné. la flèche devient verte, cela permet de réduire ou agrandir en conservant les bons rapports de proportion.

* lorsque le fichier a retrouvé sa bonne taille vous pouvez l'enregistrer et l'exporter en SVG.

  *fermeture de parenthèse*

* Vous allez désormais effectuer les **réglages de vitesse et de puissance** qui vont vous permettre de graver et/ou couper votre matériau.

* La machine lit les **dessins vectoriels et travaille par couleur**. Il est important de **paramétrer son fichier en RVG** au préalable et si l'on souhaite découper et/ou graver de différentes façons (variation de vitesse ou puissance) il faut le signaler par un changement de couleur sur le fichier. Pour choisir tout cela, sélectionner un des traits de votre projet.

* Dans l'onglet de droite appuyez sur le **+ bleu**, la couleur du trait sélectionné apparait. Dans **F** (fast) vous pouvez indiquer la vitesse découpe/gravure souhaitée et dans **%** la puissance que vous voulez y associer.

* Si vous avez d'autres couleurs il suffit de sélectionner de nouveau une couleur, de cliquer sur + et d'ajouter les réglages correspondants. Attention à sélectionner les couleurs dans l'ordre de découpe que vous souhaitez.

* Maintenant vous pouvez ouvrir la porte de la découpeuse et insérer votre matériau. La source du faisceau laser doit être située au dessus. S'il est en dehors du champ, sélectionnez l'onglet **move** et et appuyez sur l'écran pour le déplacer sur votre matériau. Vous pouvez également déplacer votre projet en sélectionnant offset. Lorsque le projet semble être à la bonne place, vous pouvez vérifier qu'il est bien dans le cadre de votre matériau en cliquant sur **les deux flèches** à coté de **run**. Le laser va effectuer le tour de votre dessin pour vous permettre de réajuster la position du matériau au besoin.
Vous allez faire le réglage manuel de la distance de découpe : prenez l'une des deux cales (11 ou 15 mm) dévissez la vis orange sur le laser et placez la cale dessous, re serrez délicatement et retirez la cale. Vous pouvez refermez la porte de la découpeuse laser. Vous êtes presque prêt à découper, plus qu'une étape !

* Avant de lancer la découpe, vous devez allumer le refroidisseur et l'extracteur d'air, rien de plus facile : il suffit d'appuyer sur les boutons et d'ouvrir la vanne d'air. Si tout devient bruyant c'est que c'est allumé.

* Une fois que tout est fait, appuyez sur le bouton **run** pour commencer la découpe !

**ASTUCE n°2 :** Cette machine est faite main et le nid d'abeille n'est pas tout jeune. En découpant, selon la matériau utilisé, il se peut qu'il soit imprégné de ces marques. Si vous souhaitez évitez cela, placez votre matériaux sur une feuille de papier.

La machine commence le processus de découpe. Il est nécessaire de **rester à côté de la machine** et de **surveiller toute la découpe** pour pouvoir contrôler tout éventuel **départ de feu**.

Lorsque la découpe est terminée la machine sonne. Attendez quelques minutes avant d'éteindre le refroidisseur (laissez évacuer la fumée). Ensuite, vous pouvez éteindre le refroidisseur et récupérer votre projet. Ne laissez pas de déchets dans la machine lorsque vous partez, éteignez les appareils et poste de travail.


## EPILOG FUSION

Comme pour la LASERSAUR, il faut scanner pour activer la machine et tourner le bouton rouge de la machine dans le sens des aiguilles d'une montre.
La découpeuse Epilog fusion est facile à prendre en main. Il suffit d'abord d'ouvrir votre document svg avec l'application **inkscape** de l'ordinateur. Ensuite :
* Vérifiez que tout est en ordre (cf le point **ASTUCE n°1**).
* Tournez la clé de la découpeuse sur **on** pour l'allumer.
* Si votre fichier est correcte, allez dans **fichier** et cliquez sur **imprimer**.
* Choisissez "l'imprimante" **Epilog** puis imprimez.
* Le logiciel de réglages de la découpeuse Epilog s'ouvre.

Après cela, pour **découper** vous devez commencer la procédure de réglages qui suit :

* En premier lieu, dans **autofocus** vous choisissez le mode **palpeur**.

* Sous **opérations** vous trouvez les différents calques de découpes organisés par couleurs.

* Vous sélectionnez un calque pour procéder à son paramétrage. Puisque nous voulons **découper**, dans **type d'opération**, sélectionnez **découpe**.

* Viennent ensuite les réglages de **vitesse**, **puissance** et **fréquence**. La fréquence doit toujours être positionnée au maximum (100%). Pour choisir vos réglages de **vitesse** et **puissance**, référez vous au carnet de tests du fablab ou effectuez vous même une grille de tests avec votre matériaux. Dans ce cas là, si votre matériaux n'a pas été testé par le Fablab, faites attention lors de la découpe pour éviter les départs de feu et ne découpez jamais un matériaux contre indiqué (cf liste).

* Dans béziers, déplacez le bouton sur marche.  

* Lorsque vous avez choisi chaque paramètre pour tous vos calques, il ne vous reste qu'à donner un nom à votre fichier dans **nom**. N'oubliez pas de replacer vos claques par ordre de découpe souhaité.

* Vous pouvez placer votre matériaux dans la découpeuse, n'hésitez pas à le caler si besoin à l'aide de Tesa, ou de poids.

* Vous pouvez vérifier ou choisir l'emplacement de votre découpe à l'aide de la caméra placée dans la découpeuse.

* Quand tout  est prêt appuyez sur imprimer.

* L'imprimante reçoit votre fichier qui apparait sur le petit écran. Sélectionnez le.

* A côté de l'imprimante se trouve le **refroidisseur**, Allumez le en appuyant sur le bouton **marche/arrêt**. Revenez à la découpeuse. **ATTENTION :**  Le refroidisseur doit être allumé au dernier moment pour éviter qu'il ne s'abime, aussitôt que la découpe est terminée éteignez le.

* Le fichier est sélectionné, et le refroidisseur allumé, vous pouvez désormais lancer votre découpe en appuyant sur **play**.

La machine commence le processus de découpe. Il est nécessaire de **rester à côté de la machine** et de **surveiller toute la découpe** pour pouvoir contrôler tout éventuel **départ de feu**.

Lorsque la découpe est terminée la machine sonne. Vous pouvez éteindre le refroidisseur. Attendez une ou deux minutes en fonction du matériaux découpé (s'il y a de la fumée par exemple) avant d'ouvrir et de récupérer votre projet. Ne laissez pas de déchets dans la machine lorsque vous partez, éteignez les appareils et poste de travail.

## DECOUPEUSE VINYLE
La machine fonctionne de la même façon que les autres découpeuses, il faut choisir les couleurs et y associer la vitesse et la puissance que l'on veut leur donner.

## CREATION D'UNE LAMPE

## Etape 1 : processus de réflexion, liaison avec l'objet de design

Pour ce module, nous avions pour exercice, de créer une lampe à partir d'une feuille de polypropylène ainsi que d'une source lumineuse de notre choix, cette lampe devait être en lien (dans la mesure du possible) avec notre projet final de design. Une autre contrainte était d'utiliser uniquement cette feuille sans la coller, sans ajouter de vis ou autre et en faisant le moins de déchets possible.

Pour le moment, mon objet n'était pas encore tranché, j'hésitais entre un chargeur de téléphone qui fonctionnera grâce à l'énergie produite quand on pédale ou alors une top case adaptée à mon porte bagage.
Lorsque je me suis lancée dans le projet de lampe avec la feuille de polypropylène, j'étais partie d'une source lumineuse issue d'une lampe frontale. Ayant retenue l'idée de la top case, je suis partie de la forme cubique. Après correction on m'a fait remarquer que cette forme ne donnerait pas une belle lumière car la matière n'est pas adaptée à la forme finale que je voulais lui donner.

## Etape 2 : reconsidération de la matière et parti pris

En rentrant chez moi, j'ai décidé de changer ma source lumineuse. Mon père m'a trouvé un câble de lampe avec un interrupteur avec une douille E17.

![](../images/module 4/1.jpg)

A partir de là j'allais revoir ma méthode d'approche et mieux comprendre le matériau. J'ai pris une feuille A4 et j'ai pensé à la façon d'utiliser le maximum de matériau. J'en suis arrivée à la conclusion qu'il fallait tout simplement se servir de toute la feuille et qu'il fallait maintenant trouver comment la transformer pour qu'elle devienne une lampe. Je me suis munie de plusieurs feuille A4, j'ai commencé par faire des entailles dans le sens de la largeur pour rendre la feuille moins rigide. Ça a fonctionné mais les entailles n'étaient pas dans le bons sens car en tordant la feuille, le résultat n'était pas vraiment concluant.

![](../images/module 4/2.png)

J'ai alors décidé de changer la direction des entailles en les plaçant dans le sens de la diagonale. De cette façon, la feuille est devenue beaucoup plus souple, et en la pliant une forme ronde se formait plus facilement. J'ai essayé de "plier" la feuille par les coins opposés, de façon à créer une sorte de tube évasé. Le résultat était plutôt concluant. Je me suis alors décidé pour des entailles espacées de 1 cm et qui démarreraient à 1cm du bord de l feuille.

![](../images/module 4/3.png)

Après ça, j'ai voulu explorer un peu plus loin les propriétés de torsion de la feuille. Pour cela, j'ai coupé la feuille A4, en deux sur la diagonale. J'ai aussi changé le sens des entailles avec un point convergeant vers l'angle droit du triangle. Et en resserrant les entailles tous les 0,5 cm environ. J'ai tordu ma feuille dans plusieurs sens, et une forme de coquillage es apparue. Pour cela, il faut relier les deux coins des petits côtés et ensuite enrouler le dernier coin. En reliant uniquement deux coins on obtient encore un autre type de lampe.

![](../images/module 4/4.png)

Pour attacher ma lampe à ma douille, j'avais d'abord réalisé des entailles sous forme de croix que j'ai corrigé par des cercles qui font le même diamètre que ma douille pour que ce soit plus propre.

## Etape 3 : réalisation de la lampe

Ma lampe est vraiment très simple à réaliser. Voici le modèle svg :

![](../images/module 4/5.png)

Et le résultat :

J'ai utilisé 3 couleurs : **bleu** pour découper, **noir** pour la dernière découpe (tour de la feuille) et le **vert** pour graver. Pour ma lampe j'ai réalisé le modèle avec les deux découpeuses, les deux ont donné un résultat intéressant mais avec leur propre défauts. Avec la LASERSAUR mes entailles étaient nettes mais le nid d'abeille était imprégné. d'où l'ASTUCE n°2, et avec l'EPILOG, il n'y a pas de marques de nid d'abeille mais la matière devient un peu blanchâtre à côté de la coupe. le design de ma lampe résidait essentiellement dans les rainures, c'est pourquoi je voulait que ces détails soient les plus propres possibles. Pour cela j'ai fait plusieurs test en faisant varier la vitesse pour que la feuille ne brule pas et ne soit pas tachée toujours en conservant une puissance moyenne. le but était de trouver la vitesse la plus lente possible qui ne crée pas de défaut (plastique fondu/brulé), en allant trop vite on risque de ne pas couper complètement le matériaux et augmenter la puissance ne donne pas une découpe nette.

**Réglages LASERSAUR :**

* vert : vitesse = 800 , puissance = 4
* bleu : vitesse = 1200, puissance = 20
* noir : vitesse = 1200, puissance = 20

**Réglages EPILOG :**

* vert : vitesse = 40, puissance = 10
* bleu : vitesse = 40, puissance = 80
* noir : vitesse = 40, puissance = 80

![](../images/module 4/6.mov)
