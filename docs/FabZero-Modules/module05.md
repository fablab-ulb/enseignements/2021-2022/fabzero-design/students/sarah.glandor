# 5. USINAGE ASSISTÉ

Cette semaine Alex nous a formé sur la **Shaper Origin**, une **CNC** (défonceuse) **portative** de précision **guidée à la main** à assistance numérique. Cet outil de fraisage de précision est révolutionnaire car l'outil est bien moins imposant qu'une CNC ordinaire et il devient donc plus abordable pour les particuliers.  
Cette machine peut lire des dessins vectoriels (SVG) grâce à un port USB.

Les fraises ont une profondeur de **43 mm** (+ 18mm) et sont maintenues par un collet de (8mm). Il existe des fraises plus fines (jusqu'à 3mm) mais il faut être vigilant car elles sont sensibles à la casse.

**Quelques précautions à connaitre :**
* Toujours travailler sur un **plan stable**.
* Bien **fixer le matériau au plan de travail**.
* Toujours **relever la fraiseuse** avant de passer à une autre forme (en appuyant sur le bouton rouge)
* Toujours **allumer l'aspirateur** à copeaux.
* **Ne pas travailler à bout de bras.**
* **Ne pas porter de vêtements amples ni de bijoux**.
* **Utiliser un plateau ou une protection de table** si l'on veut couper un matériaux de part en part pour éviter de graver les surfaces de travail.


**Les différents réglages :**
* Il est possible de choisir le type de découpe : à l'intérieur ou à l'extérieur, sur la ligne ou alors pocher/graver.
* On peut également choisir la vitesse de la fraise : **réglage manuel** de 1 à 6 (de 10 000 à 26 000 tr/min). L'option Offset permet de paramétrer le décalage du tracé de la découpe par rapport au dessin.
* On peut le faire directement sur la machine, ce qui veut dire qu'il n'est pas nécessaire de le faire sur le dessin au préalable.

Il faut savoir qu'une bonne découpe fait des **copeaux de 1mm** environ et non de la poussière ou des gros copeaux.
Pour se repérer, dans l'espace, la machine utilise une **caméra**. Il faut coller des bandes de **shaper tape** (au moins 4 dominos entiers de shaper tape) sur la surface de découpe devant la machine (espacées de 8 cm environ).

**ATTENTION à toujours positionner la machine, le shaper et le matériaux de découpe sur le même plan**.

**Se servir de la machine :**
* Vous pouvez changer la fraise avant de démarrer la machine : déssérez le mandrin et insérez la clé BTR en alternant déssérage à la main et un coup de clé. Pour allumer appuyer sur le bouton marche (en face de vous).
* Dans scan, choisir nouveau scan
* Effectuez le **Z touch** qui permet à la machine de contrôler la distance entre la fraise et le matériau.
* En appuyant sur ON, la machine est en route. **ATTENTION : il faut bien choisir où l'on veut découper (dedans, extérieur, ...).** 
