# 3. IMPRESSION 3D

## L'imprimante

**(correction Léo : ajouter description de l'imprimante 3D, son fonctionnement et son champ d'utilisation)**

## *Prusa slicer*
Cette semaine, c'est Gwen et Hélène qui nous ont fait cours. Elles nous ont expliqué comment nous allions pouvoir passer du logiciel fusion à l'objet 3D. Nous avons d'abord téléchargé le logiciel prusa slicer. L'interface du logiciel se présente comme le plateau d'une imprimante. Sur le plateau, on peut lire le nom de l'imprimante qui y est associée, on peut choisir le modèle que l'on veut. Par exemple nous ne travaillerons qu'avec les imprimantes i3mks et mk3s.

![](../images/module 3/1.png)

Ensuite nous avons ouvert notre fichier fusion qui contenait l'objet à modéliser et nous l'avons exporté au format stl. Pour commencer à travailler avec le logiciel prusa, il faut l'ouvrir et glisser le fichier stl dans la fenêtre. Notre objet apparait alors sur le plateau de l'imprimante.

![](../images/module 3/2.png)

Dans la barre d'outils à gauche, on trouve des outils qui servent à positionner notre objet sur le plateau et changer ses dimensions. Il faut positionner l'objet à zéro et sur la bonne face.
Le mode expert permet de gérer plus de paramètres de configuration.

Pour lancer une impression sur les imprimantes du Fablab, il faut définir certains paramètres.
Dans l'onglet réglages d'impression, aller dans couches et périmètre > hauteur de couches > *0,2*
Il ne faut pas choisir un remplissage de plus de 25% car l'objet n'en sera pas plus solide.
Ensuite, nous pouvons faire à peu près tous les réglages que nous jugeons utiles, par exemple, parfois il est nécessaire de mettre des supports sur notre objet car cela facilite sa réalisation.

En ce qui me concerne, voici les réglages que j'ai effectué pour réaliser l'impression de ma top case. Puisqu'il s'agissait d'un objet petits et presque sans épaisseur un mode de remplissage rapide et économe a suffit et pas besoin d'ajouter de supports non plus.

![](../images/module 3/3.png)

![](../images/module 3/4.png)

![](../images/module 3/5.png)

![](../images/module 3/6.png)

Une fois tous les réglages effectués, il faut exporter le fichier sous format *Gcode*, le mettre sur une carte SD pour que l'imprimante puisse le lire. Une fois la carte SD sur l'imprimante il faut suivre les étapes suivantes :
* nettoyer la plateforme à l'aide d'un produit à base d'acétone.
* sélectionner notre fichier sur la carte (en naviguant grâce à la molette et en appuyant pour valider)
* si l'on veut changer la couleur du fil, descendre le menu et aller dans *unload filament*, on retire le fil et on peut mettre de côté la couleur et changer. On appuie sur *load filament* , la machine chauffe, on peut commencer à introduire le fil, il faut un peu l'accompagner pour qu'il soit bien pris par les engrenages, le fil chauffé sort par la buse pour éliminer toutes traces résiduelles de la couleur précédente. La machine nous demande si la couleur est la bonne (astuce : dire non au moins une ou deux fois pour être certain). Ensuite la machine fait le reste. Il faut rester à côté de la machine pendant qu'elle réalise les trois premières couches car c'est à ce moment qu'apparaissent les premières erreurs.

J'ai réalisé mon impression en même temps que Maëlys, puisque nos temps d'impression étaient proches, nous avons mis nos deux objets sur le même plateau et les deux objets ont été imprimés en 1h27 (temps initial prévu pour Maëlys 50 min et pour moi 41 min). L'objectif d'imprimer notre objet en moins de 2h a donc été largement atteint !

![](../images/module 3/7.png)

**En ce qui concerne les limites de l'imprimante, il est vrai qu'à une plus grande échelle il aurait pu y avoir un problème d'impression dû à la réalisation des poignées qui sont des petits porte-à-faux. Pour comprendre les solutions à apporter il faudrait peut être paramétrer pour imprimer à une plus grande échelle. Lucas explique dans son rapport comment il a traité la question des angle limite sur son objet. [Voilà la page](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/lucas.melon/FabZero-Modules/module03/).**

voici le résultat de ma top case :

![](../images/module 3/8.jpg)    ![](../images/module 3/9.jpg)

Lien vers les fichiers prusa et gcode : **https://we.tl/t-ChaXS89u3t**

## *How to push from Atom ?*

La semaine dernière je n'ai pas réussi à *push* toute seule depuis Atom mais Amina m'a expliqué comment faire ! Pour *push* depuis Atom. Tout d'abord, il faut aller dans Atom et cliquer sur Git(x) en bas à droite. Ensuite aller sur *unstaged changes*, on vérifie que l'on veut push tout ce qui est répertorié et on clique sur *stage all*. Dans *commit changes* on nomme l'opération que l'on va exécuter puis l'on clique sur *commit to main*. Dans la barre du bas, on clique enfin sur *push*. Le bouton push se transforme en *fetch* une fois l'opération finie.

Pour afficher un aperçu sur Atom de la page que l'on est en train d'écrire, aller dans : packages > marckdown preview > toggle preview.  
