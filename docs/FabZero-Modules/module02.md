# 2. CONCEPTION ASSISTÉE PAR ORDINATEUR - FUSION 360

Cette semaine, nous avons commencé à modéliser avec le logiciel Fusion 360.

Nous avons commencé par découvrir les différentes interfaces du logiciel :
* cube directionnel
* barre du bas
* visual style (permet de choisir le mode de rendu : filaire, ombré, etc.)
* le curseur en bas à gauche est une sorte de time line qui permet de revenir aux versions antérieures de notre projet.
* origine permet de choisir le repère orthonormé dans lequel on travaille
* dans la barre d'outil on peut choisir le mode de travail pour le projet : design, rendu, animation ...

Pour créer un nouveau projet il suffit d'appuyer sur l'onglet + en haut à droite. Avant de commencer il faut choisir le plan dans lequel on veut travailler.

## *Création du cube du FABLAB*
Nous avons réalisé un modèle du cube du FABLAB.

![](../images/module 2/0.png)

1/ Conception du cube

On a commencé par dessiner un rectangle par le centre (en se servant de la palette d'esquisses > palette de dessin > rectangle) de *100 x 100*, c'est à dire qu'on positionne le centre du rectangle sur l'origine du repère. Puis on clique sur le profile pour le sélectionner. La commande *E* nous permettra d'extruder.

Une fenêtre apparait sur le côté droit. Pour direction on choisit *symétrique*, pour type d'étendue on choisit *tout*, on entre la valeur *100* dans distance et on choisit *nouveau corps*.

2/ Percer le cube

On positionne le cube créé sur l'une des faces. Dans *esquisse*, on sélectionne le cercle. On dessine un cercle centré sur l'origine du cube (centre du cube et centre du repère) de *50* de rayon. On sélectionne le cercle et on effectue la commande *E* et on entre la valeur *-100*. Le cube est percé.

On vérifie que l'origine n'est pas masquée pour la suite de l'exercice : dans le navigateur (à gauche, on regarde si l'onglet origine est actif : oeil non barré).

On va ensuite dans *solide > créer > réseau > réseau circulaire*. La fenêtre nous demande de sélectionner un objet, on sélectionne le tube (forme extrudée), puis on sélectionne l'axe autour duquel on va tourner (perpendiculaire à l'axe de propagation du cylindre). Dans espacement angulaire on choisit angle, valeur = *90* et quantité = *2*.

Ensuite, il faut se mettre sur une des faces du cube. Choisir *rectangle* et dessiner un rectangle en plaçant le premier point sur l'arrête gauche de la face et en finissant le rectangle dans le vide, à l'intérieur du cube. Un point noir apparait sur l'arrête. On appliquer une contrainte à ce point. Dans la barre d'outil on sélectionne le *triangle rouge* (milieu) ensuite on sélectionne le point noir et le centre du cube (point O) puis le point et le côté opposé au point. On sélectionne ensuite les côtés haut et bas du rectangle en faisant *shift*, on effectue la commande D et on entre *25*. On sélectionne la piece qui a été découpée et on appuie sur *E* on donne la valeur *-50*.

On répète cette découpe. Pour cela, on construit un axe. Mettre le cube un peu de côté de façon à voir les deux sommets les plus éloignés du cube. Dans *construire*, sélectionner *axe passant par deux points*. Après cela, tout a été répété.

On veut maintenant arrondir les angles de notre cube. Dans la time line en bas à gauche, on revient à la version où le projet est un cube. On sélectionne l'ensemble puis *congé* dans la barre d'outils, sous solide) mettre la valeur *10*. Ensuite on peut faire glisser le curseur jusque la version finale et c'est terminé.   

Tutoriels en vidéo : [ici](https://web.microsoftstream.com/video/c9e3743c-6094-442a-b774-78d94bdfc386) et [là](https://web.microsoftstream.com/video/66a278b5-48ec-4198-bed6-0bd0a863efb6)

## *Premiers pas avec fusion 360 - création de ma top case*
1/ Conception de la boite sans le couvercle

Voilà à quoi ressemble la boîte :

![](../images/index/top case 1.jpg)

Je commence par dessiner un rectangle par le centre (en me servant de la *palette d'esquisses > palette de dessin > rectangle*) de *30 x 39,5*, c'est à dire qu'on positionne le centre du rectangle sur l'origine du repère.

![](../images/module 2/1.png)

Puis on clique sur le profile pour le sélectionner. La commande *E* nous permet d'extruder. Une fenêtre apparait sur le côté droit. Pour direction on choisit *un côté*, pour type d'étendue on choisit *distance*, on entre la valeur *21* dans distance et on choisit *nouveau corps*.

![](../images/module 2/2.png)

Ensuite on se place sur l'une des deux grandes faces pour dessiner à nouveau un rectangle : on va creuser la boite. On réitère et on fait un rectangle centré sur le premier de *28,5 x 38*.

![](../images/module 2/3.png)

On appuie sur *E* et on retire *19*.

![](../images/module 2/4.png)

On crée les ouvertures pour les mains en se plaçant sur une face latérale. On trace un rectangle depuis le bord (en haut)de la boite, on lui met une contrainte *milieu* (cf méthode précédente). On choisit une largeur de *7*. Ensuite on dessine à l'intérieur un rectangle ou une ellispse de *2,5* de hauteur que l'on va venir placer à *2,5* également du bord.

![](../images/module 2/5.png)

A l'aide du curseur, on revient à la version initiale de la boîte et on applique un congé de *1* à l'ensemble. On revient à la dernière version.

![](../images/module 2/6.png)

Etapes :

![](../images/module 2/topcase.mov)

Lien vers les fichiers stl et fusion : https://we.tl/t-rXhxzXrVwg
