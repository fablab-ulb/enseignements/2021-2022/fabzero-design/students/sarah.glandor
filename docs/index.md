## à propos de moi

![](images/index/me_.jpg)

Hello!

je m'appelle **sarah**, j'ai 22 ans et je suis originaire de l'école d'architecture de **montpellier**. cette année j'ai décidé de venir étudier en **erasmus** à bxl. pas très exotique me direz vous? pourtant moi je me sens dépaysée! j'adorais manger alors ici ! c'est tous les jours x)

enfin bon, je ne suis pas venue seulement pour remplir mon bide hein. je suis venue tester une autre manière d'enseigner dans un pays qui à l'air de beaucoup ressembler au mien.

j'adore être occupée par de **nouveaux projets** : préparation de voyages, bricolage, projets scout...  

je suis capable de binge watcher une série entière en une nuit tout en étant opérationnelle le lendemain (pas vraiment 😬)

j'aimerais être une architecte qui construit des **cabanes perchées** pour rapprocher les gens de la lune 🌚

je suis sportive : j'adore le **roller** mais je déteste marcher alors je me déplace la plupart du temps à **vélo**, je ne suis pas paresseuse mais je déteste perdre mon temps 🙅

## my background

je viens d'amiens où j'ai fais deux ans de prépa maths physique 🤓 avant de finalement me réorienter vers une école d'architecture.  

## stage n°1

j'ai fais un stage avec des cabanistes, voilà ce qu'on a construit en deux semaines 🛠🏗

![](images/index/cabane.jpg)

## scout un jour, scout toujours

j'ai eu la chance d'être **cheftaine scout**, et cet été nous avons réalisé notre projet (ambitieux) de marcher de Lourdes à Biarritz et nous l'avons fait ! 🏆

![](images/index/scout.jpg)

## au tour du design

![](images/index/top case 1.jpg)

![](images/index/top case 2.jpg)

puisque je suis presque inséparable de mon vélo (sauf si mauvais temps oblige) j'ai envie de travailler sur cet objet. j'ai finalement choisi de travailler sur une **top case** adaptée à mon porte bagage. en effet, j'ai tendance à attacher mon sac directement sur le porte bagage à l'aide de tendeurs. en faisant cela mon ordinateur est soumis au chocs et risque de s'endommager. Un autre moyen est d'attacher une vieille cagette mais là vient un autre problème : les intempéries ! et ici ce n'est pas ce qu'il manque. mon idée est donc de designer une sorte de top case parfaitement adaptée au porte bagage de mon vélo, le but étant de limiter au maximum les chocs. cette top case doit pouvoir contenir mes objets du quotidien : un casque, une paire de roller, ou alors mon sac de cours.

voici une description de la bête en question 👇 :

c'est un vélo rouge de la marque MBK de l'année 1998. mes parents le l'ont offert quand je suis entrée en seconde, je l'ai donc depuis plus de 7 ans. je connais tous **ses défauts** et toutes **ses qualités** : la dynamo fait un bruit d'enfer mais c'est plutôt pratique la nuit car au moins on sait que j'arrive 🤪. passer la dernière vitesse (5e) peut être un peu difficile car il s'agit d'un modèle de manette au dessus du cintre alors il faut être délicat. cependant, l'inclinaison du guidon et la hauteur de la selle sont parfaitement réglés et je remarquerais le moindre changement ☝️.

![](images/index/velo.jpg)

La top case prendra la forme d'une caisse en bois avec un couvercle. pour le couvercle deux options :
1/ système de plaque coulissante
2/ couvercle dit classique à charnière.

Le dessous de la top case sera rainuré de façon à accueillir parfaitement le porte bagage de mon vélo. pour le design, j'imagine une caisse aux bords arrondis pour ne pas s'y accrocher lorsqu'on contourne le vélo ou qu'on le porte.  
