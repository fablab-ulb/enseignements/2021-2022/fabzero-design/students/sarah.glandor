# More about design...

# Design or not design ?

Aujourd'hui, Victor nous a fait son premier cours sur le design. Il nous a expliqué quelles étaient les clés du processus pour parvenir à la conception d'un bon objet de design. Nous avons appris que nous pouvons classer les objets en deux categories : les **outils** et les **signes**. Les outils sont les objets dits fonctionnels répondants à un besoin alors que les signes, sont les objets dits décoratifs qui ne servent à rien.

![](../images/final/cours/1.png)

Exemple : Duck - Venturi, *Enseignement à Las Vegas*

Voici les 6 étapes identifiées par Victor et son expérience pour concevoir notre objet. Ce processus permet de mieux comprendre l'obejt et les besoins associés pour pouvoir donner des solutions aux problèmes relevés.

*1/ Dé-construction de la fonction*

Il s'agit de regarder de façon détachée en portant un autre regard, sans a priori, en simple observateur (comme un anthropologue par exemple). Il faut être curieux et agir de façon méticuleuse et précise. Les bons objets sont ceux observés et dont on a compris les précédents. L'analyse nous permet de comprendre l'objet et son utilité et ses problèmes. Il ne faut pas nécessairement être designer pour faire de bons objets, le regard extérieur est très important.

*2/ Résoudre un vrai problème*

Certains objets sont ecombrants, par exemple la table. Une solution a été de créer une table qui se plie et qui puisse être rangée debout.
Cette étape est un moment essentiel de la mise en place des limites et des contraintes. En effet, les contraintes sont écrites dans un objet et font l'objet. Elles permettent de structurer la créativité et de pouvoir ensuite regarder son travail en juge impitoyable.  

![](../images/final/cours/2.jpg)
Exemple : mouvement *Chindogu*

*3/ Comprendre les dimensions spécifiques*

Lorsque que l'on pense un objet, il faut le penser en ergonome, la notion de légèreté est importante. Il faut penser au confort d'utilisation, au dimensionnement du corps. Penser aux gestes lorsque que l'on manipule l'objet à son poids, par exemple une chaise ne doit pas peser plus de 7kg. Il faut garder en tête que chaque objet possède ses propres règles de dimmensions et il faut les connaître pour faire un objet adapté à l'utilisation. Ce sont les dmensions qui accompagnent le dessin.

![](../images/final/cours/3.jpg)
Exemple : les chaises de Peter Opsvik

*4/ Esquisses / essais et erreurs / amélioration*

Pour concevoir il ne faut pas hésiter à faire et refaire, à tester, à s'enrichir d'avis extérieurs, à prendre des décisions sur la technique avec les machines à disposition. Si besoin on peut aussi faire une boucle et revenir vers les étapes précédentes. Les différents types de productions sont respectivement : les prototypes sales, les prototypes propres et enfin les prototypes fonctionnels.  

![](../images/final/cours/4.jpg)
Exemple : les chaises de Maarten Van Severen x Vitra

*5/ Adaptation, diffusion, édition*

Un objet peut être étendu à d'autres pour cela il doit être reproductible. Se posent alors les questions de la reproduction en série, de la collection. De manière générale un objet est rarement seul, c'est une composition d'éléments. On va alors l'adapter ou le décliner pour sa diffusion à un plus grand nombre, pour cela on cherchera a toujours à le réduire à sa plus simple expression.

*6/ Le nom et le logo*

Quelle est l'identité de mon objet? Il faut retranscrire de la façon la plus lisible, rapide et simple, l'objet que l'on a conçu. Cette image doit être compréhensible et clairement exposée dans un contexte qui le lie à un univers.

![](../images/final/cours/5.jpg)
Exemple : vélo Brompton


# Un tout petit grain de riz, dans un grand bol.
Dans ce cours, Victor nous a apporté les objets qui constituaient son projet : un bol pour la soupe miso, une tasse à thé, un bol en terre/ céramique pour la cuisson au four. Il nous a expliqué son chemin critique (= toutes les étapes à passer avant la finalité) à travers les 3 premières étape. Puis ensuite, quel a été le chemin technique avec la phase 4 à savoir : la réalisation de ses premiers prototypes sales.

La production à grande échelle peut suivre deux chemins :

1/ production en usine : la finalité est un objet en particulier, on va donc faire appel à un designer industriel et à un chef de production.

2/ production maison (ex : meubles) : un designer vient proposer son objet à un directeur artistique ou à un chef de production.

Ensuite, Victor nous a présenté différents objets minimalistes : deux types de porte téléphone pour vélo, un siffle et un surpantalon contre la pluie à vélo.  

L'objet qui à le plus retenu  mon attention est le **sifflet**. En effet, il a été fait à l'aide du morceau de canette. Celui qui l'a concu l'a pensé en érgonome économe. Je m'explique, le sifflet est fait avec très peu de matière et utilise l'homme pour fonctionner. En effet, les côtés du sifflet ne sont pas fermés volontairement. C'est nous qui en le tenant avec deux doigts qui allons bloquer l'air et permettre le sifflemnt. Le sifflet est donc suffisamment grand pour tenir confortablemnt entre deux doigts mais aussi très petit pour permettre une production importante de sifflets avec la même canette.

J'aime beaucoup l'idée que l'objet puisse avoir besoin de nous pour pouvoir fonctionner... à méditer.
