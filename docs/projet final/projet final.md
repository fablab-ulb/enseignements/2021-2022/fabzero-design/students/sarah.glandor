# PROJET FINAL

# **Semaine 1**

Hallo ! Vous voici enfin dans ma rubrique "projet final" ! Vous allez suivre pas à pas chacune des étapes de mon avancement.

Mon projet de design se construira autour d'un objet de mon quotidien : **mon vélo**. Indispensable pour se déplacer de jour comme de nuit, faire les courses ou simplement se balader, il possède peu d'espace propre au transport de marchandises. Cependant les plus téméraires s'en servent même pour déménager ! Alors comment rendre cet objet déjà incroyable encore plus incroyable?

![](../images/final/projet/1.mov)

Voyons comment transformer le vélo à travers les étapes présentées dans le cours *Design or not design ?* (cf rubrique *did someone say design ?*)

# *1/ Dé-construction de la fonction*

En groupe de 3 nous avons déconstruit nos objets. Cet exercice nous a permis de donner et de recevoir un avis extérieur.

**Objet :** top case pour vélo et ses dérivés: sacoches latérales, cagette...
La top case est une boite de rangement fixée sur le porte bagage d'un deux roues motorisé.

De façon générale, les deux roues (motorisés ou pas), ne sont pas optimisés pour transporter directement des objets volumineux. A la rigueur on peut transporter une personne en plus, allant jusqu'à l'adulte pour les deux roues motorisés et plutôt réservé aux enfants pour le vélo, pour des questions de poids notamment et donc de robustesse des matériaux. L'objet dont j'ai besoin doit être léger et doit pouvoir se fixer à l'arrière du vélo. Il doit également être facile à transporter, permettant le transport de grands volumes tels que : les sacs de courses, un ordinateur, des patins. Il doit également être suffisamment résistant pour porter des éléments plus lourds comme des briques de lait etc.

![](../images/final/projet/2.jpg)

# *2/ Résoudre un vrai problème*

Une tope case ou des sacoches, limitent le volume et sont des éléments coûteux : elles ne sont pas non plus adaptées pour aller faire les courses par exemple. En revanche, nous avons tous ces énormes cabas que nous achetons car nous avons fait plus de courses que prévu ! Et les utilisateurs du vélo savent que les porter sur le guidon ou notre épaule n'est pas évident ! Mon système permettra de transporter ces volumes encombrants sans modifier le contenant, ce qui évite l'achat d'un rangement spécial. Mon objet se veut être un objet d'appoint qui s'ajoute à un vélo possédant un porte bagage arrière. L'objet sera une solution pratique pour les trajets courts voire longs.

Résumé des contraintes de l'objet :
- adaptable à un vélo avec porte bagage arrière
- peu encombrant et léger
- flexible
- robuste

# *3/ Comprendre les dimensions spécifiques*

Je souhaite créer un dispositif complémentaire au porte bagage mais peu encombrant qui permette de porter n'importe quel sac (le but est de ne pas investir dans un nouveau modèle de sac et d'utiliser ceux que nous avons). De plus, ce dispositif devra prendre le moins de place possible sur le porte bagage de sorte à ce qu'il puisse toujours être utilisé (pour porter quelqu'un ou un bagage volumineux). Un dispositif discret qui ne dénature pas le vélo. Par la suite, on pourra trouver des solutions d'accroche à d'autres endroits sur le vélo.

Dimensions du porte bagage :

![](../images/final/projet/3.png)

Pour que l'objet soit adapté à mon porte bagage, j'ai procédé aux relevés dimensionnels :

Il est important de rappeler qu'un sac de courses moyen peu résister jusqu'à 25 kg. Dans un soucis d'équilibre, il s'agit de concevoir un objet qui supporterait une charge de 50 kg (critère revu à la baisse plus tard dans le projet). Vous vous demandez sûrement si mon vélo peut également supporter une telle charge : je pèse 55kg et j'ai l'habitude de transporter des personnes sur mon porte bagage (soit une charge supplémentaire entre 50 et 65 kg). Mon expérience me permet alors d'affirmer que mon vélo peut supporter un peu plus d'une centaine de kilos.

![](../images/final/projet/4.jpg)

# **Semaine 2**

# *4/ Esquisses / essais et erreurs / amélioration*

Cette semaine, je me suis lancée dans la réalisation d'un premier prototype sale. J'ai designé un objet assez brut et caricatural de ce que j'imagine. La pièce se coulisse sur le porte bagage et possède deux crochets permettant de venir soutenir deux sacs. J'ai réalisé un modèle réduit que j'ai ensuite imprimé en 3D.

![](../images/final/projet/5.png)

Etant donné que je voulais commencer par donner une forme à mon objet, je ne me suis pas attardée sur la matière. Lorsque que j'ai paramétré le fichier pour l'imprimante 3D, j'ai choisi un remplissage *courbe de Hilbert* car il donnera une souplesse à l'objet avec une densité de 5% pour économiser de matière.

J'ai récupéré mon objet le lendemain, l'impression s'était bien déroulée, pas de défauts apparents. Cependant, en enlevant les supports, le rendu n'était pas propre et l'objet n'était pas du tout flexible : je l'ai tordu volontairement et il a cassé.

![](../images/final/projet/6.png)
![](../images/final/projet/6b.png)

Pour s'installer, le dispositif, n'a pas besoin d'être flexible car le but est qu'il coulisse. Cependant, quand on lui appliquera une charge, il faudra qu'il puisse se torde sous l'effet du poids, sans casser. Il faut une flexibilité selon l'axe y.

J'ai cherché quels plastiques seraient adaptés pour obtenir l'effet désiré. Sur le site de prusa, je trouve : le TPU. Bien que ce matériau semble être la solution pour résoudre mes problèmes de torsion, il possède de nombreuses autres contraintes. Celles posant un problème sont :   
- la difficulté d'impression
- la difficulté à faire des porte-à-faux  
- les supports difficiles à imprimer
- le stringing (filaments de plastique fondu qui forment une toile)

J'ai déjà remarqué que sur mon prototype, les supports sont difficiles à retirer sans gâcher la finition de la pièce. Il s'agira peut être de proposer une solution qui n'implique pas la mise en place de supports pour avoir une belle pièce. Cette nouvelle forme doit également éviter les porte-à-faux, ce qui ne ferait que déplacer le problème sinon.  
En ce qui concerne le stringing, il est possible de corriger ce défaut souvent dû à une erreur de réglages.

En parallèle de la résolution de ces problèmes, il faut que j'essaie de trouver une forme qui utilise le moins de matière possible, qui conserve les propriétés de souplesse et de robustesse.

Voici quelques exemples d'objets résistants mais réalisés avec peu de matière :

![](../images/final/projet/7.png)
![](../images/final/projet/8.jpg)
![](../images/final/projet/9.png)
![](../images/final/projet/10.png)

Et je découvre que ça existe déjà :-(

![](../images/final/projet/11.jpg)

**Liens utiles :**
- [matériaux impression prusa](https://help.prusa3d.com/en/materials#_ga=2.80435543.1131311696.1638365374-116716399.1638365373)
- [matériaux flex](https://help.prusa3d.com/en/article/flexible-materials_2057)
- [corbeille](https://cults3d.com/fr/modèle-3d/maison/voronoibasket1)
- [atelle aérée](https://airwolf3d.com/2014/10/21/3d-printed-splint/)
- [vélo de James Novak](https://www.youtube.com/watch?v=iIQ8VktA-2c)


# **Semaine 3**
Je me suis rendue compte que l'objet existe déjà. Je vais le comparer au mien en regardant s'il répond à mes attentes pour comprendre comment je peux améliorer mon projet.

Je rappelle les contraintes auxquelles je dois répondre :
* adaptable à un vélo avec porte bagage arrière
* peu encombrant et léger
* flexible
* robuste

Je me rend compte que cet objet à trouvé une solution qui le rend adaptable à différentes tailles de porte baguage. En effet, un des côtés n'a pas de retour. Cependant, les crochets qui permettent d'accrocher les sacs ne permettent plus d'utiliser le porte bagage tel quel car ils pourraient empêcher la fixation d'objets ou même de s'assoir sur le porte baguage. Une solution serait peut être donc de créer un objet dont la surface serait plane.

De plus, l'utilisation de l'imprimante 3D pour réaliser mon objet me semble complexe, il y a beaucoup de contraintes qui semblent techniques. Après mes échanges avec le groupe, j'en suis venue à considérer la proposition de changer de matériau en prenant du bois. De fait, les contraintes du TPU étaient trop nombreuses. En prenant du bois, la question de la résistance est vite écartée. Le bois à de bonnes propriétés en traction-compression, c'est pourquoi on l'utilise en construction, notamment pour faire des poutres. Bien que mon travail ne soit pas à la même échelle, je pense que le choix est justifié.

Je peux de nouveau dresser une liste des contraintes :
* adaptable à mon vélo (dans un premier temps)
* peu encombrant = permet de transporter quelque chose ou quelqu'un sur le porte baguage
* léger
* robuste
* ne dénature pas le vélo (choix de la matière)
* discret

Partant du matériau bois, je décide de me rendre dans la salle de la CNC pour trouver des chutes. Je trouve une chute particulièrement intéressante car elles correspond aux dimensions de mon porte baguage, elle est compacte et plutôt robuste.

Dimensions de la chute :
* L = 340 mm ()à raccourcir à 325 mm)
* l = 140 mm
* e = 24 mm

C'est un bon départ pour réfléchir. Je trouve également un morceau de polystyrène plus ou moins aux mêmes dimensions. Je travaille sur le morceau de polystyrène, ce qui me permet de créer une sorte de prototype sale. Je creuse le polystyrène pour me donner une idée de la façon dont s'insèrerait le porte bagage dans la pièce. J'imagine un sytème d'enfilage/coulissant.

Ensuite, il s'agit de trouver comment fixer des sacs sans ajouter de matière à la planche. Dans un soucis d'économie toujours, quand pour l'imprimante 3D il s'agissait d'utiliser le moins de matière possible, ici, il s'agira d'enlever le moins de matière à la planche originale. Voici la première ébauche que j'imagine :
![](../images/final/projet/12.png)

En superposant ce dessin j'obtient :
![](../images/final/projet/13.png)

Ce qui me donne une idée : créer un pattern à suivre pour extruder la planche. Après quelques essais voici ce que j'obtiens.

![](../images/final/projet/14.png)

Puisque le Fablab n'ouvre pas avant mardi, en attendant j'essaye de modéliser mon objet. Voilà à quoi il ressemble :
![](../images/final/projet/15.png)
![](../images/final/projet/15b.png)

Pour le réaliser par la suite, j'aurais besoin de plusieurs des machines du FabLab : d'abord la découpe laser, une scie circulaire (ou une raboteuse) et la shaper origin.  

Après avoir taillé mes chutes avec la scie circulaire, je grave ma planche à l'aide de la découpeuse laser. Ici j'ai eu plusieurs problèmes :

1/ Bien que la planche n'était que de 240 mm de haut, elle s'est coincée entre le palpeur et le plateau de la découpeuse. La machine n'est pas cassée mais il faut toujours demander l'avis d'un professionnel de la machine si l'on a un doute sur l'un des matériaux. Pour rappel, voici la [fiche d'utilisation de la découpeuse laser Epilog Fusion](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md).

2/ matériau qui bouge pendant la découpe --> motif raté

3/ Pièce étroite lors de l'utilisation de la shaper ce qui m'a donné du fil à retordre :
- peu de shaper tape --> la machine à du mal à se repérer.
- marquage pas juste : imprécisions lors du passage de la Shaper.  


![](../images/final/projet/16.png)

![](../images/final/projet/17.png)

![](../images/final/projet/18.png)

![](../images/final/projet/19.png)

Les solutions :

* 1/ Prendre une planche moins épaisse ou utiliser une fraise adaptée, ce qui implique un achat supplémentaire.
* 2/ Fixer le matériaux, même si vous pensez qu'il est suffisamment lourd.
* 3/ Peut être que la méthode que j'ai utilisé n'était pas la bonne : il faut peut être d'abord défoncer avec la shaper et ensuite faire le travail de motif. Il faudra donc probablement prendre une planche de plus grande dimension que l'on coupera à la bonne taille ensuite.

# **Semaine 4**

Cette semaine, j'ai décidé de réessayer la découpe bois en prenant un matériau plus fin (18mm). J'inverserai aussi ma méthode de réalisation : je découperai avec la shaper, en commençant par les percements puis par les rails, je terminerai par extruder le milieu de la pièce. Ensuite je viendrai graver le motif de mon dispositif avec la découpeuse laser.  
![](../images/final/projet/20.png)

J'ai essayé deux fois. Ce fut un échec. Je n'ai pas réussi à réaliser le prototype que j'imaginais. Enfin, oui et non : les rails dans lesquels devait coulisser le porte baguage ont cassé lors du découpage du contour de la pièce. Je savais également que mon porte bagage était un peu plié et ma pièce étant parfaitement droite ne fonctionnerait pas sur toute la partie de son support. Ce constat à été déterminant dans la suite de mon parcours car il m'a permis d'abandonner sans regret cette piste.  
![](../images/final/projet/21.png)
![](../images/final/projet/22.png)
![](../images/final/projet/23.png)
![](../images/final/projet/24.png)


Pour tout de même avoir un modèle propre j'ai imprimé le modèle que j'avais dessiné sur Fusion. Il s'agit d'une maquette et non d'un prototype (la matière n'est pas assez résistante) :
![](../images/final/projet/25.png)

Car, même après m'être acharnée pour réaliser cette pièce je me rend compte que - tout même en maitrisant parfaitement la shaper (ou en utilisant une CNC classique) ou bien en choisissant un bois plus résistant - cette forme n'est pas pratique. Je me retrouve avec un objet assez lourd, pas vraiment beau, qui doit encore être retravaillé (bords à adoucir). Je rappelle que j'avais énoncé qu'il fallait un objet léger, peu encombrant, robuste, et discret. Tout le contraire de ma dernière tentative. De plus, le processus de fabrication me semble long, complexe et laborieux, je ne me vois pas poursuivre dans cette voie sans me casser la figure, c'est beaucoup d'efforts pour pas grand chose. Je me suis alors tournée vers un matériau que nous avions déjà utilisé auparavant : la feuille de polypropylène, comme me l'avait suggéré Denis. Au départ j'était dubitative, mais finalement je me rappelai que j'avais bien aimé travailler avec, et j'avais plutôt bien compris la matière, en réalité souple et solide.

La solution à mon problème m'est apparue (c'est le bon terme) quand j'étais dans le tram en rentrant de cette journée non concluante. J'ai repensé aux propriétés et aux contraintes de la feuille de polypropylène, et je me suis dit qu'il devait être possible de la plier de façon à retrouver cette accroche que j'avais caricaturée dans mon premier prototype. J'imaginais le feuille reproduisant l'enfilage sur le porte baguage. En me rappelant des contraintes de la matière je l'imaginais ensuite simplement venant s'enrouler comme une ceinture. Ensuite, j'imaginais un système de fixation qui permettrait de la rigidifier pour éviter qu'elle ne se déforme. une fois la feuille fixée, il me suffirait d'enfiler les anses des sacs et de fixer de nouveau la feuille pliée :

Prototypes sales du dispositif imaginé :

![](../images/final/projet/26.png)
![](../images/final/projet/27.png)


En dressant la liste du matériel nécessaire, je me suis rendue compte que d'autres matières pouvaient certes marcher, le choix du polypropylène ne convenant peut être pas à tous. En effet, une de mes contraintes étant : qui ne dénature pas le vélo. J'ai imaginé plusieurs gammes de "ceintures" : en feuille de polypropylène, en cuir, en ceinture tressée, etc. Je décide alors de partir à la chasse au matériel : ceinture en cuir (ou simili), boutons pression, velcro, écrous papillons et vis... Je trouve quelques bonnes adresses :

- *les tissus du chien vert*, rue du chien vert 2, 1080 (€€€)
- *brico bat*, rue du compte de flandres 53, 1080 (€)
- *les petits riens*, chaussée de Wavre 800, 1040 (€€)

Une fois tous les matériaux réunis j'ai réalisé deux prototypes : le "cheap" et le "luxe". Voilà le résultat :

![](../images/final/projet/28.png)
![](../images/final/projet/29.png)
![](../images/final/projet/30.png)

Pour le moment je suis plutôt satisfaite du résultat. Même si je pense, que les éléments de fixation doivent encore être améliorés, optimisés. Par exemple les boutons en pressions fonctionnent mais peut être qu'il en faudrait des plus gros, j'imagine également les remplacer par des boutons aimantés. Une autre de mes questions concerne la nécessité  ou non d'ajouter des renforts dans la longueur de la pièce. Piste encore à vérifier.

Même si le dernier prototype est plutôt convainquant, une question est comment attacher un sac dont la anse est trop longue sans faire de boucle avec. Je me lance sur une piste qui consisterait à ajouter une autre ceinture complémentaire sur le côté de la roue.  

![](../images/final/projet/52.png)

# *5/ adaptation, diffusion, édition*
Ici voilà l'évolution des mes prototypes.
![](../images/final/projet/31.png)
![](../images/final/projet/32.png)

Analysons et les et comparons les entre eux :

1/ plutôt bien mais ne répond pas à la contrainte : peu encombrant car on perd un peu l'usage du porte baguage à cause des crochets.

2/ trop épais difficile à réaliser avec les outils du FabLab

3/ long et laborieux pas robuste, difficile à mettre en oeuvre

4/ simple à réaliser, la machine 3D l'imprime sans problème cependant compter plus de 7h d'impression pour une pièce mais non solide.

5/ très simple à faire, rapide, peu cher et répond à toutes les contraintes établies.

Pour le moment, si je devais produire à grande échelle l'un de mes prototypes, le dernier serait idéal car il utilise vraiment très peu de matière, je l'imagine aussi très bien en "kit à monter soi même" et personnalisable.


# **Semaine 5**
Compte rendu du jury :
Le jury du 16 décembre m'a permis de porter un regard nouveau sur mon projet et de mettre en lumière d'autres problèmes. Les pistes d'améliorations du jury concernaient la matière : lesquelles? je m'étais aventurée sur la piste du polypropylène et du cuir. Plusieurs options s'offrent à moi finallment :
- les tissus
- le cuir ??
- la chambre à air ?
- la sangle

Aussi, plusieurs autres pistes d'amélioration étaient en jeu :

D'abord, comment éviter que le sac ne se coince dans les rayons ? En effet, comme expliqué précédemment, mon vélo possède des gardes boue latéraux, ce qui permet déjà d'éviter ce phénomène. Mais, tous les vélos ne possèdent pas ce dispositif, il fallait donc que je trouve un moyen de parer ce problème.  

Ensuite, vient la question du maintient latéral du sac une fois attaché. Si le sac n'est pas assez lourd, en tout cas avec le point d'attache que je propose cela pose problème, il va se balancer d'avant en arrière lorsque l'on freine par exemple.

Pour répondre à ces questionnements j'imaginais déjà plusieurs solutions mais on me proposait ensuite toujours autre chose, le meilleur conseil à sûrement été de penser un objet qui se ferait en une seule pièce et qui viendrait porter et aussi bloquer les sacs sans gardes-boue latéraux.

D'autres questions sur lesquelles me pencher et peut être trouver une solution :
- Quels types de sacs porter ? tous les sacs? Se pose alors la question de la matière de ma ceinture et de la charge qu'elle sera capable de supporter.
- Comment faire quand il pleut ?


Après avoir rencontré tous les membres du jury j'avais une vague idée de l'objet qu'il me fallait réaliser mais sans avoir cerné sa matière. Il est vrai qu'à ce moment tout ce que j'avais en tête était une sorte de harnais pour porte bagage arrière, pas très sexy n'est ce pas? Cependant il fallait bien que je me lance pour contrer tous les problèmes.   

- Pour résumer :

Voilà les pistes d'amélioration :
- 1/ lutter contre l'accrochage aux rayons
- 2/ lutter contre le balancement
- 3/ choix des matériaux (up-cyling)
- 4/ adaptabilité : quels sacs?
- 5/ quid de la pluie  

# **Semaine 6**

Cette semaine j'ai commencé le prototype sale de ma ceinture 2.0. Je suis rentrée chez mes parents, ainsi j'ai pu retrouver mon père, mon "expert vélo". J'ai récupéré de vielles chambre-à-air pour tester cette matière. Effectivement c'est très résistant tout en restant très élastique.

1ère étape : lutter contre l'accrochage aux rayons

Comme à mon habitude, je me suis directement mis au bricolage pour expérimenter sans faire de réelle recherche sur papier.

J'ai commencé par utiliser uniquement des chambres-à-air (j'en ai pris une plus grosse, pour un pneu de mobylette par exemple). Mais ça ne marchait pas vraiment : c'est difficile d'enfiler les deux chambre-à-air, car ça ne coulisse pas, de plus, c'est difficile à installer.
![](../images/final/projet/33.png)

Le deuxième dispositif que j'ai essayé se composait de la ceinture en cuir proposée pour le jury, dans laquelle j'ai essayé de passer la chambre-à-air. Elle y coulisse mieux c'est déjà ça. La chambre à air vient sur la partie latérale du vélo pour limiter l'accès aux rayons. Cependant, il y a un autre problème : ce harnais devient très compliqué à mettre en place et je perd l'efficacité d'installation que j'avais gagné à l'époque du pré-jury. De plus, la reprise de charges au sein de mon dispositif devait se faire au niveau des attaches (vis ou pressions) alors qu'ici, c'est la matière même qui porte. Je crains qu'avec le temps la ceinture ne s'abime car désormais elle est fragilisée par les entailles permettant le passage de la sangle.
![](../images/final/projet/34.jpg)

Comme je ne suis pas très convaincue, je décide de me laisser plus de temps pour reprendre le projet à tête reposée après les fêtes et les révisions.

# **Semaine 7**
Semaine de pause é_é

# **Semaine 8**
En revenant au Fablab, j'ai pu retrouver quelques camarades, ce qui m'a permis d'exposer mes problèmes et d'avoir un avis extérieur. Ensemble, on se rend compte qu'il y a une façon plus facile de passer la sangle latérale (en la fixant sous la ceinture qui encercle le porte bagage notamment). En proposant le projet à Victor, il est d'accord sur le fait que cet assemblage rend le projet plus compliqué, de plus, pour lutter pour le balancement le projet tel quel à besoin d'encore plus de "sangles". Ca marche mais ça ne marche pas. Il faut simplifier. On reprend la boucle depuis le début : qu'est ce que je veux faire ?  Un porte sacs pour vélos. Quels sacs ? Voilà le hic. Tous les sacs ? Il me conseille de me concentrer uniquement sur les tote bags par exemple car léger, sacs répandus, sert à diffuser des messages, personnels, c'est un type de sac qui rassemble déjà un grand groupe de personnes. J'accepte directement l'idée, le discours me plait. Ensuite, Victor me fait remarquer que la sangle latérale à l'air largement suffisante si je travaille avec les totes bags, en effet leur poids sera toujours limité et leur volume aussi. Nous essayons d'attacher les sacs de Gwen et Hélène de manière inversée : on cale le sac à droite et la anse bascule sur la gauche. Normalement, puisque le haut du sac ou la anse repose sur la ligne du porte baguage, cela crée une ligne d'appui qui évitera ou minimisera le balancement. Alors que dans la version que je proposais au pré jury, le sac étant accroché en un point il se balançait. Il s'agit désormais de trouver un moyen de fixer le sac sur la sangle qui contourne le vélo à l'aide d'un crochet par exemple.

Avec toutes ces pistes en tête, je décide de partir une nouvelle fois à la chasse au matériel. L'équipe du Fablab me donne plusieurs adresses :

- *R-use Fabrik*, Rue du Relais 21, 1050 (€)
- *Accessoires Leduc*, Bd Maurice Lemonnier 120, 1000 (€)
- *Goldfingers*, Bd Anspach 146, 1000 (€€)

J'ai vraiment adoré cette étape du projet, cette fois je savais exactement ce que j'allais faire, je ne savais pas quel crochet j'allais utiliser mais je savais que je le reconnaitrais quand je le verrai.

Je suis d'abord allée chez R-Use Fabrik, il s'agit d'une ASBL qui récupère du matériel de couture et qui les revend à un prix très abordable. L'ASBL propose aussi des cours de couture pour tous les âges ou de louer l'espace machine à coudre. Ici, j'ai trouvé plusieurs matériaux, dont la plupart de mes sangles, et d'autres tissus de récupération : vieux gilet jaune, tissu réfléchissant etc.

Ensuite, je me suis rendue chez Accessoires Leduc pour les œillets et les boutons pression. Ici, je me suis procurée d'autres sangles, le vendeur ma expliqué qu'ils étaient les promoteurs de sangles en Europe et qu'ils avaient énormément de modèles, autant pour la couture que pour la manutention. J'ai trouvé un modèle très intéressant avec une bande réfléchissante au milieu. Les vendeurs étaient vraiment accueillants et ils m'ont conseillés sur le modèle d'œillet pour mon projet. Ils n'avaient pas le crochet que je cherchais, en effet, leurs modèles de crochets étaient plutôt adaptés à la maroquinerie.

La dernière mercerie à laquelle je me suis rendue était donc Goldfingers. Ici, j'ai trouvé de l'élastique en bande ainsi que les fameux crochets !!
Cette journée d'emplettes était vraiment très productive j'ai réussi à trouver tout le matériel dont j'avais besoin il ne me restait plus qu'à fabriquer mes fameuses ceintures ! Pour cela il faudrait que je me forme pour la brodeuse numérique / machine à coudre. Ce sera pour la semaine prochaine !   

J'ai comparé plusieurs tote bags car la question de l'attache du sac dépendra de la longueur de anse :

Une piste pour la suite du projet sera la confection d'un tote bag spécialement conçu pour le dispositif. J'imagine un sac doublé réversible. Que l'ont peut mettre en mode nuit (face réfléchissante) et un mode jour plus sobre en blanc.  

Un autre paramètre à prendre en compte sera la distance d'axe à axe en passant par le porte bagage. Là encore, plusieurs modèles entrent en jeu. En effet, les vélos ont une taille de pneu variant en fonction du modèle : VTC, VTT, BMX, vélo junior. Leur diamètre (en pouces) varie de 12 à 24 pour les juniors et de 26 à de 29 pour les vélos adultes (3 tailles 26, 27,5 et 29). Aujourd'hui, il existe des nouveaux modèles de jantes pouvant aller jusqu'à 32 pouces !
Pour le projet je me concentre sur la gamme dite 'normale' de vélos soit allant jusqu'à 29 pouces. Je me suis donc munie de mon mètre de couture et je suis partie à la recherche de vélos de toutes tailles. Car en fonction des modèles, bien que la hauteur de pneu soit la même, seule la position du porte bagage sera déterminante dans le calcul de la longueur de ceinture. Il ne faudrait pas qu'on se retrouve avec une sangle trop courte qui ne convient pas à un 29 pouces ! Voici un tableau récapitulatif de mesures exhaustives sur des vélos lambdas. Mon éventail de recherche s'est ouvert aux vélos du hall de mon immeuble, de la collection de vélos de mon père et de modèles tirés au sort dans la rue. De cette façon, j'ai pu obtenir un panel de mesures assez varié malgré le faible échantillon, je me suis servie de de ces mesures pour imaginer les cas critiques (plus petit et plus grand) pour dimensionner parfaitement ma sangle.

Echantillon du tableau des mesures réalisées sur des vélos randoms :

![](../images/final/projet/35.png)

Dès lors, j'imagine une astuce résidant dans l'utilisation d'un élastique qui permettra de rendre adaptable, premièrement, à tout type de porte baguage (nous avons vu précédemment qu'ils peuvent être très étroits ou très larges). Deuxièmement, l'élastique permettra d'aller chercher l'axe de la roue plus loin pour les plus grands vélos.
A ce stade du projet, deux options s'offrent à moi :

1/ La création d'une ceinture unique à découper en fonction de la taille de son vélo. A la manière des semelles à couper ou des vêtements à couper :

![](../images/final/projet/36.png)

![](../images/final/projet/37.png)

2/ Ou bien, une gamme de trois tailles de ceintures, survolant l'éventail des tailles de roues de vélo.


A termes, il s'agira très certainement, d'opter pour l'options 2, dans un soucis d'économie de matériaux et de temps.


En parallèle, je me suis penchée sur la question du nom et du logo.  

# *6/ Le nom et le logo*

J'ai discuté de mon projet avec mes parents et ils m'ont fait remarquer que la particularité du projet résidait dans sa simplicité. Il fallait que ça se retranscrive dans le slogan ou alors dans le nom, quelque chose avec "easy" pour faire court. Je me suis dit vendu ! Je l'appellerais *EASY*. Mais pourquoi EASY ? qu'est ce qui est "easy" dans mon projet ? et bien on porte des sacs facilement. Parfait, j'obtient quelque chose comme *easy carry*. Un objet qui permet de transporter facilement permet de rouler serein : *easy ride*. Et voilà ! Ensuite, la question du logo, j'ai pensé qu'il serait intéressant que le nom soit le logo, c'est pourquoi j'ai abrégé le mot EASY par *EZ*. J'imagine sous les deux lettre le sloggan *easy carry, easy ride*. J'ai trouvé ma "marque" plutôt facilement. Ça suffira pour le moment, peut être que je réfléchirais au type de police et au logo plus tard, mais ce sont des détails.

# **Semaine 9**

Cette semaine, Cécile m'a formé à la brodeuse numérique / machine à coudre. Elle est justement en train de rédiger un manuel d'utilisation pour le fablab alors elle a pu me donner quelques tips ! Une des consignes du projet est d'utiliser au moins une des machines du fablab. Mon objet ne nécessitera pas en réalité les machines numériques pour sa création. Cependant, je peux me servir de la brodeuse numérique pour inscrire la marque de l'objet sur les sangles.

*Petit tuto brodeuse numérique :*

![](../images/final/projet/38.png)

Pour broder, il faut exporter les fichiers sous les formats pes ou dst. Si on utilise inkscape, il faut installer le plug in insketch qui permet au logiciel de transformer le fichier en un format pour broder. Pour broder des lettres, un peut comme l'imprimante 3D, on peut choisir la technique de remplissage, en modifiant l'angle par exemple, d'autres réglages existent et sont à tester !
Lorsque l'on brode il y a deux scénarios possibles :
- 1/ on invente la forme ou
- 2/ la forme est existante

Lorsque la forme existe il n'y a pas de problème. Quand elle n'existe pas il faut suivre la procédure suivante :
clic droit > convertir en chemin > objet en chemin. Ici il est possible de régler la densité, ensuite cliquer sur apply and quit > export.

Pour broder il faudra installer le tissus dans le cadre de broderie. On place le tissu du côté que l'on veut broder. On remarque deux petites flèches qui doivent se pointer mutuellement, c'est là qu'on va fermer le cadre.

La machine s'allume sur le coté droit. En cliquant sur l'écran ou les boutons on peut choisir le programme que l'ont souhaite utiliser.

Pour coudre/broder, il faut s'assurer que l'aiguille est bien installée ainsi que l'embout à broder, et enfin, que le fil est bien armé (ouvrir le boitier en haut et faire passer le fil selon le schéma numéroté de 1 à 8, abaisser la guillotine en 9). Ensuite, placer le cadre sous l'aiguille. Pour effectuer des modifications toujours relever l'aiguille et le pied de biche. Pour relever l'aiguille appuyer sur le bouton avec son dessin. Pour relever le pied de biche relever le levier, derrière la machine. Il existe différentes tailles de cadre, en fonction de la taille du cadre, le rail où il s'attache se déplace (position maximum tout à gauche).

Il est aussi possible de broder des lettres ou des motifs qui sont déjà présents dans la machine à broder. La prise en main est très intuitive.  

Pour passer en mode couture il faut utiliser une aiguille différente et changer le plateau (plus petit, tirer vers la gauche pour l'enlever) et installer le pied à coudre.

********************************************************************************************************************************

Voilà plus ou moins ce que j'ai appris avant de commencer à réaliser mes ceintures. Evidemment, j'ai pris la main au fur et à mesure en cousant et Cécile et Gwen on été très patientes car ce n'était pas si simple !

Avant de vous expliquer comment j'ai procédé pour réaliser mes ceintures, voici la liste du matériel pour un porte sacs :

- 1 sangle de 1050 mm (1000 + 50 pour la marge )
- une bande élastique de de même largeur que la sangle
- du fil assorti à la sangle
- 10 / 12 œillets
- 2 mousquetons ronds

![](../images/final/projet/39.png)
![](../images/final/projet/40.png)
![](../images/final/projet/41.png)

liste des outils :

- des ciseaux
- une brodeuse / machine à coudre
- un feutre
- un marteau
- une latte ou un mètre de couture
- un briquet

pour l'installation :

- une clé à molette ou clé de la taille de l'écrou de votre axe de roue.


Processus de réalisation :

D'après le tableau des mesures réalisées sur les vélos, il faudra compter une longueur d'axe à axe d'un mètre maximum. La ceinture sera donc composée de deux matières : une sangle coupée en 2 reliée par une bande élastique. Une fois assemblés ils devront faire une longueur d'un mètre. Il conviendra de faire la sangle légèrement plus petite pour éviter le flottement du dispositif (pour les plus grands vélos en particulier), il faudra que pour chaque taille de vélo on puisse tendre légèrement l'élastique pour une meilleure adhérence au porte baguage. En ce qui concerne l'attache du sac j'avais d'abord pensé à une boucle cousue dans laquelle on mettrait le mousqueton. En le faisant j'ai préféré travailler dans l'espace pur de la sangle, ce qui apportait une forme de simplicité épurée, sans ajout de matière. Après discussion avec Victor, on se rend compte que l'abandon de la boucle est un peu dommage, je trouve une référence que je vais essayer d'appliquer pour des futurs modèles.

![](../images/final/projet/43.png)

![](../images/final/projet/44.png)


J'ai donc cousu un premier côté de la ceinture, pour cela, prendre une longueur de sangle (50 cm environ) et une bande élastique de 15 cm. Les coudre ensemble à environ 2-3 cm du bord à l'aide de la machine à coudre. Ensuite rabattre chaque reste de tissus et coudre en dessous de la matière correspondante. Passer 2 fois. Répéter l'opération de l'autre côté de l'élastique. Ensuite, avec un feutre, marquer la sangle tous les 9 cm. Trouer au niveau des marquages et brûler les trous pour éviter que la sangle ne s'effiloche. A chaque trou installer un œillet.

![](../images/final/projet/42.png)

J'ai chargé la ceinture à 5 kg toute une nuit pour vérifier la robustesse des coutures, et ça a résisté. J'ai seulement remarqué que l'élastique c'était détendu de 1 cm au cours de la nuit. C'est normal car la charge était suspendue. Il n'y a pas de risque de déformation de l'élastique pour son utilisation de porte sacs.

![](../images/final/projet/51.png)

Installation du dispositif sur le vélo :

Pour installer la ceinture, munissez vous d'une clé de la taille de votre écrou d'axe de roue arrière (ou d'une clé à molette). **AVERTISSEMENT :** l'installation requiert de maitriser l'installation d'une roue arrière de vélo, si nécessaire, rapprochez vous d'un expert. Dévissez l'écrou d'un côté et insérez l'œillet correspondant à votre longueur de roue. Dévissez le deuxième écrou seulement après avoir resserré le premier. Procédez de la même façon pour l'autre côté.

![](../images/final/projet/45.png)

Installation des tote bags :

Prenez votre tote bag et placez le sur un côté du vélo, passez la anse du sac sur le côté opposé et fixez la avec l'un des mousquetons que vous attacherez à l'œillet correspondant à la longueur de la anse. Il est possible d'attacher un autre tote bag en répétant la même opération.

![](../images/final/projet/46.png)


Après avoir confectionné la ceinture je l'ai testée toute la semaine pour vérifier que le dispositif était fonctionnel et pratique. C'était validé !

![](../images/final/projet/47.mov)

# **Semaine 10**

Après avoir fait mon propre prototype, je souhaitais qu'il soit plus "professionnel". C'est pourquoi je me suis rapprochée de Nicole, une habitante de mon village, experte en couture. Après lui avoir expliqué le principe du projet elle m'a confectionné un prototype plus propre de la ceinture, sa couture était bien nette ! Elle n'a pas travaillé uniquement à la machine à coudre mais d'abord à la main. Une fois la ceinture cousu il ne me restait plus qu'à y mettre les œillets ! La maitrise de la machine à coudre est primordiale pour que l'objet fini soit beau. De fait, le détail des coutures sera en quelque sorte la master piece du projet.

![](../images/final/projet/48.png)


# *5/ adaptation, diffusion, édition*

![](../images/final/projet/50.png)

La solution présentée se veut être universelle car découpable et adaptable. Comme expliqué plus haut, à termes il s'agira de créer certainement des gammes de tailles pour éviter le gaspillage de matière. Pour cela, on se rend compte qu'il faudra travailler à la main, le temps de réalisation d'une ceinture prend un peu moins d'une heure (le plus gros étant les 6 coutures et les 12 œillets). La ceinture est évidemment reproductible en FabLab mais également chez soi (on peut très bien imaginer se passer de machine à coudre, bien que moins confortable).


Ensuite, selon les goûts et les couleurs, on peut très bien imaginer choisir sa sangle et également le style d'attache-mousquetons. Par exemple, cela pourrait être le modèle œillet, le modèle boucle répétée et peut être avec un budget plus important une boucle coulissante. Ainsi, les modèles pourraient être variés et personnalisés.

Selon moi, après avoir exploré toutes les pistes des ceintures, la suite logique du projet serait de proposer une gamme de tote bags.

# *6/ Le nom et le logo*

EZ - easy carry, easy ride.

![](../images/final/projet/49.mov)

#Conclusion

En conclusion, le projet est très satisfaisant car il répond aux contraintes énoncées en début de parcours, pour rappel :
- adaptable à mon vélo (dans un premier temps)
- peut encombrant
- léger
- robuste
- ne dénature pas le vélo
- discret  
- stabilité et sécurité des sacs
- matériaux optimums
- adaptabilité : quels sacs?

On peut dire qu'il a largement dépassé les exigences. D'abord il est adaptable à tous type de vélo 'classic' avec porte bagage. En comparaison avec les premiers essais, la matière utilisée à été réduite d'environ 90% : l'objet pèse quelque grammes et tient dans la paume d'une main. Il n'en est pas moins très solide. La position verticale de la sangle sécurise l'accès aux rayons. L'accroche inversée cale le sac contre le porte bagage et évite le balancement permettant sa stabilité. Son caractère personnalisable, en fait un objet qui plaira à tous. La sangle est fine et reste discrète (mais possibilité de couleurs flashy si désiré !). La modification du type de sacs en cours du projet n'est finalement pas dérangeante car elle est plus réaliste et permet à chacun de s'exprimer en "exposant" ses tote bags.




Merci d'avoir lu !




REMERCIEMENTS

Merci à tous mes camarades de Design, à l'équipe du FABLAB (Gwen, Hélène, Cécile, Victor et Denis), à mes parents, à ceux qui vivaient cette QA par procuration. A tous ceux qui ont su porter un regard critique sur mon projet tout au long du semestre pour me permettre de l'enrichir, d'aller encore plus loin. Ce travail de recherche m'a permis de repousser les limites de la créativité et m'a rappelé l'importance de la rigueur dans le processus de projet pour le mener à bon terme. C'était une chouette aventure et elle n'est pas terminée :)

--------------------------------------------------------------------------------------------------------------------------------
